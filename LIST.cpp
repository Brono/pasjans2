#include "stdafx.h"
#include "LIST.h"
#include <iostream>
#include <stdlib.h>
#include <ctime>
using namespace std;

LIST::LIST()
{
	srand(time(NULL));
}


LIST::~LIST()
{
	//Nic nie robi.
}

void LIST::InitList(int col, int wart)
{
	head = new LIST_ELEM;
	head->karta.kolor = col;
	head->karta.warto�� = wart;
	head->next = nullptr;
	head->prev = nullptr;
	tail = head;
}

void LIST::WypiszListe()
{
	LIST_ELEM *toDisp;
	toDisp = head;
	while (nullptr != toDisp)
	{
		cout << "Kolor karty: " << toDisp->karta.kolor << "\t" << "Wartosc karty: " << toDisp->karta.warto�� << endl;
		toDisp = toDisp->next;
	}
}

void LIST::Wstaw(int nr, int col, int wart)
{
	LIST_ELEM *wsk;
	LIST_ELEM *newElem;
	wsk = head;
	for (int i = 0; i < nr; i++)
	{
		if (nullptr != wsk)
			wsk = wsk->next;
		else
			cout << "Lista nie posiada tylu elementow!" << endl;
	}
	newElem = new LIST_ELEM;
	newElem->next = wsk->next;
	newElem->prev = wsk;
	newElem->karta.kolor = col;
	newElem->karta.warto�� = wart;
	if (newElem->next != nullptr)
		newElem->next->prev = newElem;
	wsk->next = newElem;
	if (newElem->next == nullptr)
		tail = newElem;
}

void LIST::UsunListe()
{
	LIST_ELEM *toDelete;
	toDelete = head;
	while (nullptr != toDelete)
	{

		toDelete = toDelete->next;
		delete toDelete->prev;

	}
}

void LIST::DopiszNaKoniec(int col, int wart)
{
	LIST_ELEM *nowy = new LIST_ELEM;
	nowy->karta.kolor = col;
	nowy->karta.warto�� = wart;
	nowy->next = nullptr;
	nowy->prev = tail;
	tail->next = nowy;
	tail = nowy;
}

void LIST::DopiszNaPoczatek(int col, int wart)
{
	LIST_ELEM *nowy = new LIST_ELEM;
	nowy->karta.kolor = col;
	nowy->karta.warto�� = wart;
	nowy->prev = nullptr;
	nowy->next = head;
	head->prev = nowy;
	head = nowy;
}

void LIST::Dopisz(LIST_ELEM *wsk)
{
	wsk->prev = tail;
	wsk->next = nullptr;
	tail->next = wsk;
	tail = wsk;
}

LIST_ELEM* LIST::Wydaj(int index)
{
	LIST_ELEM * toGive;
	toGive = head;
	for (int i = 0; i < index; i++)
	{
		if (nullptr != toGive->next)
			toGive = toGive->next;
		else
			break;
	}
	if (nullptr != toGive->next)
		toGive->next->prev = toGive->prev;
	else
		tail = toGive->prev;

	if (nullptr != toGive->prev)
		toGive->prev->next = toGive->next;
	else
		head = toGive->next;

	return toGive;
}

void LIST::Tasuj()
{
	int num = 0;
	LIST_ELEM *wsk;
	wsk = head;

	while (nullptr != wsk->next)
	{
		num++;
		wsk = wsk->next;
	}

	for (int i = 0; i < 1000; i++)
	{
		Dopisz(Wydaj(rand() % num));
	}
}

void LIST::StworzTalie()
{
	LIST_ELEM *wsk;
	wsk = new LIST_ELEM;
	head = wsk;
	for (int col = 1; col <= 4; col++){
		for (int num = 1; num <= 13; num++){
			wsk->karta.kolor = col;
			wsk->karta.warto�� = num;
			if (num == 13 && col == 4){
				wsk->next = nullptr;
			}else{
				wsk->next = new LIST_ELEM;
				wsk->next->prev = wsk;
				wsk = wsk->next;
			}
			
		}
	}
	head->prev = nullptr;
	tail = wsk;
}