#pragma once

struct KARTA{
	int kolor;
	int warto��;
};

struct LIST_ELEM{
	KARTA karta;
	LIST_ELEM *next;
	LIST_ELEM *prev;
};

class LIST
{
public:
	LIST();
	~LIST();
	void InitList(int col, int wart);
	void Wstaw(int nr, int col, int wart);
	void DopiszNaKoniec(int col, int wart);
	void DopiszNaPoczatek(int col, int wart);
	void Dopisz(LIST_ELEM *wsk);
	LIST_ELEM* Wydaj(int index);
	LIST_ELEM* Wypisz(int index);
	void Tasuj();
	void WypiszListe();
	void UsunListe();
	void StworzTalie();

private:
	LIST_ELEM *head;
	LIST_ELEM *tail;
};

