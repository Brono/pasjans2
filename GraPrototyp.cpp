#include <iostream>
#include <windows.h>
#include <ctime>
#include <stdlib.h>
#include <conio.h>
using namespace std;

typedef struct karta{
    int kolor;
    int numer;
};

struct taliaKarta
{
    karta card;
	taliaKarta *next;
	taliaKarta *prev;
};

struct talia
{
    talia();
	~talia();
	//void InitList(int col, int wart);
	//void Wstaw(int nr, int col, int wart);
	//void DopiszNaKoniec(int col, int wart);
	//void DopiszNaPoczatek(int col, int wart);
	void Dopisz(taliaKarta *wsk);
	void DopiszKarte(karta *wsk);
	taliaKarta* Wydaj(int index);
	//LIST_ELEM* Wypisz(int index);
	void Tasuj();
	void WypiszListe();
	taliaKarta* WydajOstat();
	//void UsunListe();
	void StworzTalie();
	void VisualDisp();

	taliaKarta *head;
	taliaKarta *tail;
};



void GoToXY(int x, int y)
{
	COORD c;
	c.X = x;
	c.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
}

void WysKart(int numer, int symbol, int col, int row, int aka, int space = 1)// int space)
{
    row = row * 3;
    GoToXY(col*5+(space*col)+aka, row);
    cout << (char)218 << (char)196 << (char)196 << (char)196 << (char)191 << endl;
    row++;
    GoToXY(col*5+(space*col)+aka, row);
    cout << (char)179;
    switch(numer)
    {
    case 13:
        cout << "K ";
        break;
    case 12:
        cout << "Q ";
        break;
    case 11:
        cout << "J ";
        break;
    case 1:
        cout << "A ";
        break;
    case 2:
        cout << "2 ";
        break;
    case 3:
        cout << "3 ";
        break;
    case 4:
        cout << "4 ";
        break;
    case 5:
        cout << "5 ";
        break;
    case 6:
        cout << "6 ";
        break;
    case 7:
        cout << "7 ";
        break;
    case 8:
        cout << "8 ";
        break;
    case 9:
        cout << "9 ";
        break;
    case 10:
        cout << "10";
        break;
    default:
        cout << "##";
    }
    switch(symbol)
    {
    case 1:
        cout << (char)3;
        break;
    case 2:
        cout << (char)4;
        break;
    case 3:
        cout << (char)5;
        break;
    case 4:
        cout << (char)6;
        break;
    default:
        cout << "#";
        break;
    }
    cout << (char)179 << endl;
    row++;
    GoToXY(col*5+(space*col)+aka, row);
    cout << (char)192 << char(196) << (char)196 << (char)196 << (char)217 << endl;
}



struct piramida{
    karta *t1[3];
    karta *t2[6];
    karta *t3[9];
    karta *t4[10];
    void Init();
    void Disp();
    void Delete();
    void Controls(int &a, int &b);
    karta* VisualPointer(int , int);
    karta* GetCard(int nrTabl, int index);
};

struct game
{
    piramida P;
    talia T;
    void Init();
    void Loop();
    void Controls(int &a, int &b);
};

int main()
{
    /*piramida P;
    P.Init();
    /////////////////////////////////
    int a,b;
    a = 3;
    b = 0;
    karta *pKarta;
    int l = 0;
    /////////////////////////////////////
    for(;;){
        system("cls");
        P.Disp();
        pKarta = P.VisualPointer(a, b);
        if(pKarta != NULL)
        cout << pKarta->kolor << " " << pKarta->numer << endl;
        P.Controls(a, b);


    }

    P.Delete();*/
    game G;
    G.Init();
    G.Loop();

    return 0;
}

void piramida::Init()
{
    for(int i = 0; i<3; i++)
    {
        t1[i] = new karta;
        //t1[i]->kolor = i;
        //t1[i]->numer = i;
    }
    for(int i = 0; i<6; i++)
    {
        t2[i] = new karta;
        //t2[i]->kolor = i;
        //t2[i]->numer = i;
    }
    for(int i = 0; i<9; i++)
    {
        t3[i] = new karta;
        //t3[i]->kolor = i;
        //t3[i]->numer = i;
    }
    for(int i = 0; i<10; i++)
    {
        t4[i] = new karta;
        //t4[i]->kolor = i;
        //t4[i]->numer = i;
    }
}

void piramida::Disp()
{
    for(int i = 0; i<3; i++)
    {
        if( t1[i] != NULL )
        {
            if( t2[i*2] == NULL && t2[i*2+1] == NULL )
            {
                WysKart(t1[i]->numer, t1[i]->kolor, i, 0, 15, 7);
            }
            else
            {
                WysKart(0, 0, i, 0, 15, 7);
            }
        }
    }
    for(int i = 0; i<6; i++)
    {
        if( t2[i] != NULL )
        {
            if( i%2 == 0 )
            {
                if( t3[static_cast<int>(i*1.5)] == NULL && t3[static_cast<int>(i*1.5)+1] == NULL )
                {
                    WysKart(t2[i]->numer, t2[i]->kolor, i, 1, 6, 4);
                }
                else
                {
                    WysKart(0, 0, i, 1, 6, 4);
                }
            }
            else
            {
                if( t3[i+(i/2)] == NULL && t3[i+(i/2)+1] == NULL )
                {
                    WysKart(t2[i]->numer, t2[i]->kolor, i, 1, 6, 4);
                }
                else
                {
                    WysKart(0, 0, i, 1, 6, 4);
                }
            }
        }
    }
    for(int i = 0; i<9; i++)
    {
            if( t3[i] != NULL )
            {
                if( t4[i] == NULL && t4[i+1] == NULL )
                {
                    WysKart(t3[i]->numer, t3[i]->kolor, i, 2, 3);
                }
                else
                {
                    WysKart(0, 0, i, 2, 3);
                }
            }
    }
    for(int i = 0; i<10; i++)
    {
            if( t4[i] != NULL )
            {
                WysKart(t4[i]->numer, t4[i]->kolor, i, 3, 1);
            }
    }
}

void piramida::Delete()
{
    for(int i = 0; i<3; i++)
    {
        delete t1[i];
    }
    for(int i = 0; i<6; i++)
    {
        delete t2[i];
    }
    for(int i = 0; i<9; i++)
    {
        delete t3[i];
    }
    for(int i = 0; i<10; i++)
    {
        delete t4[i];
    }
}

karta* piramida::VisualPointer(int row, int col)
{
    int a = col;
    int b = row;
    switch(row)
    {
    case 0:
        col = (col * 12) + 14;
        break;
    case 1:
        col = (col * 9) + 5;
        break;
    case 2:
        col = (col * 6) + 2;
        break;
    case 3:
        col = col * 6;
        break;
    }
    row = row * 3;

    GoToXY(col, row);
    cout << ">";
    GoToXY(col, row+1);
    cout << ">";
    GoToXY(col, row+2);
    cout << ">";
    GoToXY(col+6, row);
    cout << "<";
    GoToXY(col+6, row+1);
    cout << "<";
    GoToXY(col+6, row+2);
    cout << "<";

    GoToXY(0, 14);
    switch(b)
    {
    case 0:
        return t1[a];
        //cout << t1[a]->kolor << " " << t1[a]->numer << endl;
        break;
    case 1:
        return t2[a];
        //cout << t2[a]->kolor << " " << t2[a]->numer << endl;
        break;
    case 2:
        return t3[a];
        //cout << t3[a]->kolor << " " << t3[a]->numer << endl;
        break;
    case 3:
        return t4[a];
        //cout << t4[a]->kolor << " " << t4[a]->numer << endl;
        break;
    }
}

void piramida::Controls(int &a, int &b)
{
    if(kbhit)
        {
            char key = getch();
            switch(key){
            case 'a':
                b--;
                break;

            case 'd':
                b++;
                break;

            case 'w':
                a--;
                break;

            case 's':
                a++;
                break;
            case 'g':
                switch(a)
                {
                case 0:
                    delete t1[b];
                    t1[b] = NULL;
                    //cout << t1[a]->kolor << " " << t1[a]->numer << endl;
                    break;
                case 1:
                    delete t2[b];
                    t2[b] = NULL;
                    //cout << t2[a]->kolor << " " << t2[a]->numer << endl;
                    break;
                case 2:
                    delete t3[b];
                    t3[b] = NULL;
                    //cout << t3[a]->kolor << " " << t3[a]->numer << endl;
                    break;
                case 3:
                    delete t4[b];
                    t4[b] = NULL;
                    //cout << t4[a]->kolor << " " << t4[a]->numer << endl;
                    break;
                }

            }
        }
}

void talia::StworzTalie()
{
	taliaKarta *wsk;
	wsk = new taliaKarta;
	head = wsk;
	for (int col = 1; col <= 4; col++){
		for (int num = 1; num <= 13; num++){
			wsk->card.kolor = col;
			wsk->card.numer = num;
			if (num == 13 && col == 4){
				wsk->next = NULL;
			}else{
				wsk->next = new taliaKarta;
				wsk->next->prev = wsk;
				wsk = wsk->next;
			}

		}
	}
	head->prev = NULL;
	tail = wsk;
}

karta* piramida::GetCard(int nrTabl, int index)
{
    switch(nrTabl)
    {
    case 1:
        return t1[index];
        break;
    case 2:
        return t2[index];
        break;
    case 3:
        return t3[index];
        break;
    case 4:
        return t4[index];
        break;
    }
}

void talia::Tasuj()
{
    srand(time(NULL));
	int num = 0;
	taliaKarta *wsk;
	wsk = head;

	while (NULL != wsk->next)
	{
		num++;
		wsk = wsk->next;
	}

	for (int i = 0; i < 1000; i++)
	{
		Dopisz(Wydaj(rand() % num));
	}
}

taliaKarta* talia::Wydaj(int index)
{
	taliaKarta * toGive;
	toGive = head;
	for (int i = 0; i < index; i++)
	{
		if (NULL != toGive->next)
			toGive = toGive->next;
		else
			break;
	}
	if (NULL != toGive->next)
		toGive->next->prev = toGive->prev;
	else
		tail = toGive->prev;

	if (NULL != toGive->prev)
		toGive->prev->next = toGive->next;
	else
		head = toGive->next;

	return toGive;
}

taliaKarta* talia::WydajOstat()
{
    taliaKarta * toGive;
    toGive = tail;
    tail->prev->next = NULL;
    tail = tail->prev;
}

void talia::Dopisz(taliaKarta *wsk)
{
	wsk->prev = tail;
	wsk->next = NULL;
	tail->next = wsk;
	tail = wsk;
}

void talia::DopiszKarte(karta *wsk)
{
    taliaKarta *newWsk = new taliaKarta;
    newWsk->card.kolor = wsk->kolor;
    newWsk->card.numer = wsk->numer;
    newWsk->prev = tail;
	newWsk->next = NULL;
	tail->next = newWsk;
	tail = newWsk;
}

void talia::WypiszListe()
{
	taliaKarta *toDisp;
	toDisp = head;
	while (NULL != toDisp)
	{
		cout << "Kolor karty: " << toDisp->card.kolor << "\t" << "Wartosc karty: " << toDisp->card.numer << endl;
		toDisp = toDisp->next;
	}
}

talia::talia()
{

}

talia::~talia()
{

}

void talia::VisualDisp()
{
    taliaKarta *wsk;
	wsk = head;
    int num = 0;
    while (NULL != wsk->next)
	{
		num++;
		wsk = wsk->next;
	};
	cout << "Kart w tali: " << num << " X. karta na szczycie: ";
    WysKart(tail->card.numer, tail->card.kolor, 7, 4, 0);
}

void game::Init()
{
    karta wsk;
    P.Init();
    T.StworzTalie();
    T.Tasuj();
    for(int i = 0; i<3; i++)
    {
        wsk = T.Wydaj(i)->card;
        P.t1[i]->kolor = wsk.kolor;
        P.t1[i]->numer = wsk.numer;
    }
    for(int i = 0; i<6; i++)
    {
        wsk = T.Wydaj(i)->card;
        P.t2[i]->kolor = wsk.kolor;
        P.t2[i]->numer = wsk.numer;
    }
    for(int i = 0; i<10; i++)
    {
        wsk = T.Wydaj(i)->card;
        P.t4[i]->kolor = wsk.kolor;
        P.t4[i]->numer = wsk.numer;
    }
    for(int i = 0; i<9; i++)
    {
        wsk = T.Wydaj(i)->card;
        P.t3[i]->kolor = wsk.kolor;
        P.t3[i]->numer = wsk.numer;
    }
}

void game::Loop()
{
    int a,b;
    a = 3;
    b = 0;
    for(;;){
        system("cls");
        P.Disp();
        P.VisualPointer(a, b);
        T.VisualDisp();
        this->Controls(a, b);
    }
}

void game::Controls(int &a, int &b)
{
    if(kbhit)
        {
            char key = getch();
            switch(key){
            case 'a':
                b--;
                break;

            case 'd':
                b++;
                break;

            case 'w':
                a--;
                break;

            case 's':
                a++;
                break;
            case 32:
                int nrKZT;
                nrKZT = T.tail->card.numer;

                switch(a)
                {
                case 0:
                    if(P.t1[b]->numer == nrKZT+1 || P.t1[b]->numer == nrKZT-1)
                    {
                        delete T.WydajOstat();
                        T.DopiszKarte(P.t1[b]);
                        //delete P.t1[b];
                        P.t1[b] = NULL;
                    }
                    else
                    {
                        if( P.t1[b]->numer == 13 && nrKZT == 1 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t1[b]);
                            //delete P.t1[b];
                            P.t1[b] = NULL;
                        }
                        if( P.t1[b]->numer == 1 && nrKZT == 13 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t1[b]);
                            //delete P.t1[b];
                            P.t1[b] = NULL;
                        }
                    }
                    //delete P.t1[b];
                    //P.t1[b] = NULL;
                    //cout << t1[a]->kolor << " " << t1[a]->numer << endl;
                    break;
                case 1:
                    if(P.t2[b]->numer == nrKZT+1 || P.t2[b]->numer == nrKZT-1)
                    {
                        delete T.WydajOstat();
                        T.DopiszKarte(P.t2[b]);
                        //delete P.t1[b];
                        P.t2[b] = NULL;
                    }
                    else
                    {
                        if( P.t2[b]->numer == 13 && nrKZT == 1 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t2[b]);
                            //delete P.t1[b];
                            P.t2[b] = NULL;
                        }
                        if( P.t2[b]->numer == 1 && nrKZT == 13 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t2[b]);
                            //delete P.t1[b];
                            P.t2[b] = NULL;
                        }
                    }
                    //cout << t2[a]->kolor << " " << t2[a]->numer << endl;
                    break;
                case 2:
                    if(P.t3[b]->numer == nrKZT+1 || P.t3[b]->numer == nrKZT-1)
                    {
                        delete T.WydajOstat();
                        T.DopiszKarte(P.t3[b]);
                        //delete P.t1[b];
                        P.t3[b] = NULL;
                    }
                    else
                    {
                        if( P.t3[b]->numer == 13 && nrKZT == 1 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t3[b]);
                            //delete P.t1[b];
                            P.t3[b] = NULL;
                        }
                        if( P.t3[b]->numer == 1 && nrKZT == 13 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t3[b]);
                            //delete P.t1[b];
                            P.t3[b] = NULL;
                        }
                    }
                    //cout << t3[a]->kolor << " " << t3[a]->numer << endl;
                    break;
                case 3:
                    if(P.t4[b]->numer == nrKZT+1 || P.t4[b]->numer == nrKZT-1)
                    {
                        delete T.WydajOstat();
                        T.DopiszKarte(P.t4[b]);
                        //delete P.t1[b];
                        P.t4[b] = NULL;
                    }
                    else
                    {
                        if( P.t4[b]->numer == 13 && nrKZT == 1 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t4[b]);
                            //delete P.t1[b];
                            P.t4[b] = NULL;
                        }
                        if( P.t4[b]->numer == 1 && nrKZT == 13 )
                        {
                            delete T.WydajOstat();
                            T.DopiszKarte(P.t4[b]);
                            //delete P.t1[b];
                            P.t4[b] = NULL;
                        }
                    }
                    //cout << t4[a]->kolor << " " << t4[a]->numer << endl;
                    break;
                }
                break;

            case 'x':
                delete T.WydajOstat();
                break;
            }
        }
}
